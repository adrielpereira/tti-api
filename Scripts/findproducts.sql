SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[FindProducts] @Filter nvarchar(30)
AS
SELECT Product.Id, Product.Name, Product.Detail,Product.Price, Product.CategoryId  FROM Product 
JOIN Category  ON Category.Id = Product.CategoryId
WHERE Product.Name Like  '%' + @Filter + '%' OR Product.Detail Like  '%' + @Filter + '%' OR Category.Name Like  '%' + @Filter + '%'
GO