# TTIProducts Test

TTIProducts its a simple REST API with a CRUD for Products, Categories and Subcategories.
The SPA(Front End) project access by this link  [TTI SPA](https://gitlab.com/adrielpereira/tti-spa.git)

### Installation
Clone this repo
```sh
git clone https://gitlab.com/adrielpereira/tti-api.git
```

Inside TTiStore.Data folder run in console
```sh
dotnet ef migrations add InitialCreate
```

After that run this command to generate the database
```sh
dotnet ef database update
```

And to add the [FindProducts] stored procedure you can find the script 'findproducts.sql' inside scripts folder.

>If you wanna generate the entire database by script you can find the script 'database.sql' inside scripts folder.

### Tech

  - Microsoft .Net Core 2.2
  - Entity Framework Core 2.2.1
  - Swashbuckle Swagger 4.0.1
  - AutoMapper DI 4.0.1
  - Microsoft SQL Server
  - Repository Pattern



