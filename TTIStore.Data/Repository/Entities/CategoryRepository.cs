﻿using TTIStore.Data.Context;
using TTIStore.Data.Models;
using TTIStore.Data.Repository.Interfaces;

namespace TTIStore.Data.Repository.Entities
{
    public class CategoryRepository : BaseRepository<Category>, ICategoryRepository
    {
        public CategoryRepository(DataContext context) : base(context) { }
    }
}
