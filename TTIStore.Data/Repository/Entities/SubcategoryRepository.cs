﻿using TTIStore.Data.Context;
using TTIStore.Data.Models;
using TTIStore.Data.Repository.Interfaces;

namespace TTIStore.Data.Repository.Entities
{
    public class SubcategoryRepository : BaseRepository<Subcategory>, ISubcategoryRepository
    {
        public SubcategoryRepository(DataContext context) : base(context) { }
    }
}
