﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using TTIStore.Data.Context;
using TTIStore.Data.Helpers;
using TTIStore.Data.Models;
using TTIStore.Data.Repository.Interfaces;

namespace TTIStore.Data.Repository.Entities
{
    public class ProductRepository : BaseRepository<Product>, IProductRepository
    {
        public ProductRepository(DataContext context) : base(context) { }

        public async Task<PagedList<Product>> GetAllbyCategory(long categoryId, int pageNumber, int pageSize)
        {
            var model = DbContexto.Product.Where(x => x.CategoryId == categoryId);

            return await PagedList<Product>.CreateAsync(model, pageNumber, pageSize);
        }

        public async Task<PagedList<Product>> GetAllbySubcategory(long subCategoryId, int pageNumber, int pageSize)
        {
            var model = DbContexto.Product.Where(x => x.ProductSubcategories.Where(s => s.SubcategoryId == subCategoryId).Count() > 0);

            return await PagedList<Product>.CreateAsync(model, pageNumber, pageSize);
        }

        public async Task<PagedList<Product>> SearchByNameAndDescription(string filter, int pageNumber, int pageSize)
        {
            var model = DbContexto.Product.Where(x => x.Name.Contains(filter) || x.Detail.Contains(filter));

            return await PagedList<Product>.CreateAsync(model, pageNumber, pageSize);
        }

        public async Task<PagedList<Product>> SearchByNameAndDescriptionSP(string filter, int pageNumber, int pageSize)
        {
           
            var parameters = new[] { new SqlParameter("Filter", filter) };

            var model = DbContexto.Product.FromSql("[dbo].FindProducts @Filter", parameters);

            return await PagedList<Product>.CreateAsync(model, pageNumber, pageSize);
        }
    }
}
