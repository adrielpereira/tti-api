﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTIStore.Data.Context;
using TTIStore.Data.Helpers;
using TTIStore.Data.Repository.Interfaces;

namespace TTIStore.Data.Repository.Entities
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class
    {
        protected DataContext DbContexto = null;
        DbSet<TEntity> _dbSet;

        public BaseRepository(DataContext ctx)
        {
            DbContexto = ctx;
            _dbSet = DbContexto.Set<TEntity>();
        }

        public async Task Add(TEntity obj)
        {
            await _dbSet.AddAsync(obj);
        }

        public async Task<PagedList<TEntity>> GetAll(int pageNumber, int pageSize)
        {
            return await PagedList<TEntity>.CreateAsync(_dbSet, pageNumber, pageSize);
        }

        public async Task<TEntity> GetById(long id)
        {
            return await _dbSet.FindAsync(id);
        }

        public void Remove(TEntity obj)
        {
            _dbSet.Remove(obj);
        }

        public void Update(TEntity obj)
        {
            _dbSet.Update(obj);
        }

        public void Dispose()
        {
            DbContexto.Dispose();
            _dbSet = null;
        }
    }
}
