﻿using TTIStore.Data.Context;
using TTIStore.Data.Models;
using TTIStore.Data.Repository.Interfaces;

namespace TTIStore.Data.Repository.Entities
{
    public class ProductSubcategoryRepository : BaseRepository<ProductSubcategory>, IProductSubcategoryRepository
    {
        public ProductSubcategoryRepository(DataContext context) : base(context) { }
    }
}
