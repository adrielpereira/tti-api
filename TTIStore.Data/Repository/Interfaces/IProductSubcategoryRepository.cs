﻿using TTIStore.Data.Models;

namespace TTIStore.Data.Repository.Interfaces
{
    public interface IProductSubcategoryRepository : IBaseRepository<ProductSubcategory>
    {
    }
}
