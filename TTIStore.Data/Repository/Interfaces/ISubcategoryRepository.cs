﻿using TTIStore.Data.Models;

namespace TTIStore.Data.Repository.Interfaces
{
    public interface ISubcategoryRepository : IBaseRepository<Subcategory>
    {
    }
}
