﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTIStore.Data.Helpers;

namespace TTIStore.Data.Repository.Interfaces
{
    public interface IBaseRepository<TEntity> where TEntity : class
    {
        Task Add(TEntity obj);

        Task<TEntity> GetById(long id);

        Task<PagedList<TEntity>> GetAll(int pageNumber, int pageSize);

        void Update(TEntity obj);

        void Remove(TEntity obj);
    }
}
