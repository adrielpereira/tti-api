﻿using System;
using System.Collections.Generic;
using System.Text;
using TTIStore.Data.Models;

namespace TTIStore.Data.Repository.Interfaces
{
    public interface ICategoryRepository : IBaseRepository<Category>
    {
    }
}
