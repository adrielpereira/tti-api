﻿using System.Threading.Tasks;
using TTIStore.Data.Helpers;
using TTIStore.Data.Models;
using TTIStore.Data.Repository.Interfaces;

namespace TTIStore.Data.Repository.Interfaces
{
    public interface IProductRepository : IBaseRepository<Product>
    {
        Task<PagedList<Product>> GetAllbyCategory(long categoryId, int pageNumber, int pageSize);

        Task<PagedList<Product>> GetAllbySubcategory(long categoryId, int pageNumber, int pageSize);

        Task<PagedList<Product>> SearchByNameAndDescription(string filter, int pageNumber, int pageSize);

        Task<PagedList<Product>> SearchByNameAndDescriptionSP(string filter, int pageNumber, int pageSize);
    }
}
