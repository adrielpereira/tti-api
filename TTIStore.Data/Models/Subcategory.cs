﻿using System.Collections.Generic;

namespace TTIStore.Data.Models
{
    public class Subcategory : BaseModel
    {
        public string Name { get; set; }

        public long CategoryId { get; set; }
       
        public virtual Category Category { get; set; }

        public virtual ICollection<ProductSubcategory> ProductSubcategories { get; set; }

    }
}
