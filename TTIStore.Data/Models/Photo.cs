﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TTIStore.Data.Models
{
    public class Photo : BaseModel
    {
        public string Url { get; set; }

        public long ProductId { get; set; }

        public virtual Product Product { get; set; }
    }
}
