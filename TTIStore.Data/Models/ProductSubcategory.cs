﻿namespace TTIStore.Data.Models
{
    public class ProductSubcategory : BaseModel
    {
        public long ProductId { get; set; }

        public long SubcategoryId { get; set; }

        public virtual Product Product { get; set; }

        public virtual Subcategory Subcategory { get; set; }
    }
}
