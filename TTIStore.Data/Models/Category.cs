﻿using System.Collections.Generic;

namespace TTIStore.Data.Models
{
    public class Category : BaseModel
    {
        public string Name { get; set; }

        public virtual ICollection<Subcategory> Subcategories { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}
