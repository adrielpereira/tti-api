﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TTIStore.Data.Models
{
    public class Product : BaseModel
    {
        public string Name { get; set; }

        public decimal Price { get; set; }

        public string Detail { get; set; }        

        public long CategoryId { get; set; }

        public virtual Category Category { get; set; }

        public virtual ICollection<Photo> Photos { get; set; }

        public virtual ICollection<ProductSubcategory> ProductSubcategories { get; set; }


    }
}
