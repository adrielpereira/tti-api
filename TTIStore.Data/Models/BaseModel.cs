﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TTIStore.Data.Models
{
    public class BaseModel
    {
        [Key]
        public long Id { get; set; }
    }
}
