﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TTIStore.Data.Models;

namespace TTIStore.Data.Context.Mapping
{
    public class CategoryMap : IEntityTypeConfiguration<Category>
    {
        public void Configure(EntityTypeBuilder<Category> builder)
        {
            builder.ToTable("Category");

            builder.Property(x => x.Name).HasMaxLength(150);

            builder.HasMany(x => x.Subcategories).WithOne(x => x.Category).OnDelete(DeleteBehavior.Restrict);

        }
    }
}
