﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TTIStore.Data.Models;

namespace TTIStore.Data.Context.Mapping
{
    public class ProductMap : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.ToTable("Product");

            builder.Property(x => x.Price).HasColumnType("decimal(17,2)");

            builder.Property(x => x.Name).HasMaxLength(150);

            builder.HasOne(x => x.Category).WithMany(x => x.Products).OnDelete(DeleteBehavior.Restrict);

            builder.HasMany(x => x.Photos).WithOne(x => x.Product).OnDelete(DeleteBehavior.Restrict);

            builder.HasMany(x => x.ProductSubcategories).WithOne(x => x.Product).OnDelete(DeleteBehavior.Restrict);
        }
    }
}
