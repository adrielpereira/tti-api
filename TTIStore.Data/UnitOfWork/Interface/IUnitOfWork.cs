﻿using System.Threading.Tasks;
using TTIStore.Data.Repository.Interfaces;

namespace TTIStore.Data.UnityOfWork.Interface
{
    public interface IUnitOfWork
    {
        IProductRepository ProductRepository { get; }
        ICategoryRepository CategoryRepository { get; }
        ISubcategoryRepository SubcategoryRepository { get; }
        IProductSubcategoryRepository ProductSubcategoryRepository { get; }

        Task<bool> Commit();
    }
}
