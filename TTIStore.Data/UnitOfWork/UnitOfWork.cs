﻿using System;
using System.Threading.Tasks;
using TTIStore.Data.Context;
using TTIStore.Data.Repository.Entities;
using TTIStore.Data.Repository.Interfaces;
using TTIStore.Data.UnityOfWork.Interface;

namespace TTIStore.Data.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private DataContext _contexto = null;
        private ProductRepository _product = null;
        private CategoryRepository _category = null;
        private SubcategoryRepository _subCategory = null;
        private ProductSubcategoryRepository _productSubcategory = null;

        private bool disposed = false;

        public UnitOfWork(DataContext context)
        {
            _contexto = context;
        }

        public IProductRepository ProductRepository
        {
            get
            {
                if (_product == null)
                    _product = new ProductRepository(_contexto);
                return _product;
            }
        }

        public ICategoryRepository CategoryRepository
        {
            get
            {
                if (_category == null)
                    _category = new CategoryRepository(_contexto);
                return _category;
            }
        }

        public ISubcategoryRepository SubcategoryRepository
        {
            get
            {
                if (_subCategory == null)
                    _subCategory = new SubcategoryRepository(_contexto);
                return _subCategory;
            }
        }

        public IProductSubcategoryRepository ProductSubcategoryRepository
        {
            get
            {
                if (_productSubcategory == null)
                    _productSubcategory = new ProductSubcategoryRepository(_contexto);
                return _productSubcategory;
            }
        }

        public async Task<bool> Commit()
        {
            if (await _contexto.SaveChangesAsync() > 0)
                return true;
            else
                return false;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _contexto.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
