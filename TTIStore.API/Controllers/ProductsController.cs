﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using TTIStore.API.Business;
using TTIStore.API.DTOs;
using TTIStore.API.Helpers;
using TTIStore.API.Models;
using TTIStore.Data.Helpers;
using TTIStore.Data.Models;
using TTIStore.Data.UnityOfWork.Interface;

namespace TTIStore.API.Controllers
{
    [Route("api/products")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IUnitOfWork _uow;
        private readonly IMapper _mapper;
        private readonly ProductLogic _productLogic;

        public ProductsController(IUnitOfWork uowContext, IMapper mapper)
        {
            this._uow = uowContext;
            this._mapper = mapper;
            this._productLogic = new ProductLogic();
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery]UserParams userParams)
        {
            PagedList<Product> model = null;

            if(string.IsNullOrEmpty(userParams.Search))
                model = await _uow.ProductRepository.GetAll(userParams.PageNumber, userParams.PageSize);
            else
                model = await _uow.ProductRepository.SearchByNameAndDescriptionSP(userParams.Search, userParams.PageNumber, userParams.PageSize);

            var mapped = _mapper.Map<IEnumerable<ProductDTO>>(model);

            mapped = _productLogic.OrderBy(userParams, mapped);

            Response.AddPagination(model.CurrentPage, model.PageSize, model.TotalCount, model.TotalPages);

            return Ok(mapped);
        }

        // GET: api/Products/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var model = await _uow.ProductRepository.GetById(id);

            if (model == null)
                return NotFound();

            var mapped = _mapper.Map<ProductDTO>(model);

            return Ok(mapped);
        }

        // POST: api/Products
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ProductViewModel model)
        {
            if (ModelState.IsValid)
            {
                var product = _mapper.Map<Product>(model);

                await _uow.ProductRepository.Add(product);

                if (await _uow.Commit())
                {
                    return Ok(_mapper.Map<ProductViewModel>(product));
                }
            }

            return BadRequest();
        }

        // POST: api/Products/{id}/Subcategories
        [HttpPost("/subcategories")]
        public async Task<IActionResult> PostSubcategory([FromBody]ProductSubcategoryViewModel model)
        {
            if (ModelState.IsValid)
            {
                var produtSubcategory = _mapper.Map<ProductSubcategory>(model);

                await _uow.ProductSubcategoryRepository.Add(produtSubcategory);

                if (await _uow.Commit())
                {
                    return Ok(_mapper.Map<ProductViewModel>(produtSubcategory));
                }
            }

            return BadRequest();
        }

        // PUT: api/Products/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] ProductViewModel model)
        {
            if (id != model.Id)
                return BadRequest();

            var nModel = _mapper.Map<Product>(model);

            _uow.ProductRepository.Update(nModel);

            if (await _uow.Commit())
            {
                var productDto = await _uow.ProductRepository.GetById(id);
                return Ok(_mapper.Map<ProductDTO>(productDto));
            }
                

            return BadRequest();
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var model = await _uow.ProductRepository.GetById(id);

            if (model == null)
                return NotFound();

            _uow.ProductRepository.Remove(model);

            if (await _uow.Commit())
                return Ok(model);

            return BadRequest();
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}/subcategories/{productSubcategoryId}")]
        public async Task<IActionResult> Delete(int id, long productSubcategoryId)
        {
            var model = await _uow.ProductSubcategoryRepository.GetById(id);

            if (model == null)
                return NotFound();

            _uow.ProductSubcategoryRepository.Remove(model);

            if (await _uow.Commit())
                return NoContent();

            return BadRequest();
        }
    }
}
