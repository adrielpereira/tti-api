﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TTIStore.API.DTOs;
using TTIStore.API.Helpers;
using TTIStore.API.Models;
using TTIStore.Data.Models;
using TTIStore.Data.UnityOfWork.Interface;

namespace TTIStore.API.Controllers
{
    [Route("api/subcategories")]
    [ApiController]
    public class SubcategoriesController : ControllerBase
    {
        private readonly IUnitOfWork _uow;
        private readonly IMapper _mapper;

        public SubcategoriesController(IUnitOfWork uowContext, IMapper mapper)
        {
            this._uow = uowContext;
            this._mapper = mapper;
        }

        // GET: api/Subcategories
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery]UserParams userParams)
        {
            var model = await _uow.SubcategoryRepository.GetAll(userParams.PageNumber, userParams.PageSize);

            var mapped = _mapper.Map<IEnumerable<SubcategoryDTO>>(model);

            Response.AddPagination(model.CurrentPage, model.PageSize, model.TotalCount, model.TotalPages);

            return Ok(mapped);
        }

        [HttpGet("{id}/products")]
        public async Task<IActionResult> GetProducts(long id, [FromQuery]UserParams userParams)
        {
            var model = await _uow.ProductRepository.GetAllbySubcategory(id, userParams.PageNumber, userParams.PageSize);

            if (model == null || model.Count() == 0)
                return NoContent();

            var mapped = _mapper.Map<IEnumerable<ProductDTO>>(model);

            Response.AddPagination(model.CurrentPage, model.PageSize, model.TotalCount, model.TotalPages);

            return Ok(mapped);
        }

        // GET: api/Subcategories/5
        [HttpGet("{id}", Name = "Get")]
        public async Task<IActionResult> Get(int id)
        {
            var model = await _uow.SubcategoryRepository.GetById(id);

            if (model == null)
                return NotFound();

            var mapped = _mapper.Map<SubcategoryDTO>(model);

            return Ok(mapped);
        }


        // POST: api/Subcategories
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] SubcategoryViewModel model)
        {
            if (ModelState.IsValid)
            {
                var subcategory = _mapper.Map<Subcategory>(model);

                await _uow.SubcategoryRepository.Add(subcategory);

                if (await _uow.Commit())
                {
                    return Ok(_mapper.Map<SubcategoryViewModel>(subcategory));
                }
            }

            return BadRequest();
        }

        // PUT: api/Subcategories/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] SubcategoryViewModel model)
        {
            if (id != model.Id)
                return BadRequest();

            _uow.SubcategoryRepository.Update(_mapper.Map<Subcategory>(model));

            if (await _uow.Commit())
                return Ok(model);


            return BadRequest();
        }

        // DELETE: api/Subcategories/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var model = await _uow.SubcategoryRepository.GetById(id);

            if (model == null)
                return NotFound();

            _uow.SubcategoryRepository.Remove(model);

            if (await _uow.Commit())
                return NoContent();

            return BadRequest();
        }
    }
}
