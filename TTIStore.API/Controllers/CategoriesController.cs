﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using TTIStore.API.DTOs;
using TTIStore.API.Helpers;
using TTIStore.Data.Models;
using TTIStore.Data.UnityOfWork.Interface;

namespace TTIStore.API.Controllers
{
    [Route("api/categories")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        private readonly IUnitOfWork _uow;
        private readonly IMapper _mapper;

        public CategoriesController(IUnitOfWork uowContext, IMapper mapper)
        {
            this._uow = uowContext;
            this._mapper = mapper;
        }


        // GET: api/Categories
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery]UserParams userParams)
        {
            var model = await _uow.CategoryRepository.GetAll(userParams.PageNumber, userParams.PageSize);

            var mapped = _mapper.Map<IEnumerable<CategoryDTO>>(model);

            Response.AddPagination(model.CurrentPage, model.PageSize, model.TotalCount, model.TotalPages);

            return Ok(mapped);
        }

        [HttpGet("{id}/products")]
        public async Task<IActionResult> GetProducts(int id, [FromQuery]UserParams userParams)
        {
            var model = await _uow.ProductRepository.GetAllbyCategory(id, userParams.PageNumber, userParams.PageSize);

            var mapped = _mapper.Map<IEnumerable<ProductDTO>>(model);

            Response.AddPagination(model.CurrentPage, model.PageSize, model.TotalCount, model.TotalPages);

            return Ok(mapped);
        }

        // GET: api/Categories/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var model = await _uow.CategoryRepository.GetById(id);

            if (model == null)
                return NotFound();

            var mapped = _mapper.Map<CategoryDTO>(model);

            return Ok(mapped);
        }

        // POST: api/Categories
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CategoryDTO model)
        {
            if (ModelState.IsValid)
            {
                var category = _mapper.Map<Category>(model);

                await _uow.CategoryRepository.Add(category);

                if (await _uow.Commit())
                {
                    return Ok(_mapper.Map<CategoryDTO>(category));
                }
            }

            return BadRequest();
        }

        // PUT: api/Categories/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] CategoryDTO model)
        {
            if (id != model.Id)
            {
                return BadRequest();
            }

            _uow.CategoryRepository.Update(_mapper.Map<Category>(model));

            if (await _uow.Commit())
            {
                return Ok(model);
            }

            return BadRequest();
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult>  Delete(int id)
        {
            var model = await _uow.CategoryRepository.GetById(id);

            if (model == null)
                return NotFound();

            var products = await _uow.ProductRepository.GetAllbyCategory(id, 1, 10);

            if (products != null && products.Count > 0)
                return BadRequest("You need to delete products from this category first");

            _uow.CategoryRepository.Remove(model);

            if (await _uow.Commit())
            {
                return Ok(model);
            }

            return BadRequest();
        }


    }
}
