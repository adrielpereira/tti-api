﻿namespace TTIStore.API.Models
{
    public class SubcategoryViewModel
    {
        public long? Id { get; set; }

        public string Name { get; set; }
         
        public long CategoryId { get; set; }
    }
}
