﻿namespace TTIStore.API.Models
{
    public class ProductSubcategoryViewModel
    {
        public long? Id { get; set; }

        public long ProductId { get; set; }

        public long SubcategoryId { get; set; }

    }
}
