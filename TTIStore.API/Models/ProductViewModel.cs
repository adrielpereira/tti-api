﻿using System;

namespace TTIStore.API.Models
{
    public class ProductViewModel
    {
        public long? Id { get; set; }

        public string Name { get; set; }

        public string Detail { get; set; }

        public decimal Price { get; set; }

        public long CategoryId { get; set; }

    }
}
