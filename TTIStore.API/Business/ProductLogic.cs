﻿using System;
using System.Collections.Generic;
using System.Linq;
using TTIStore.API.DTOs;
using TTIStore.API.Helpers;

namespace TTIStore.API.Business
{
    public class ProductLogic
    {
        public IEnumerable<ProductDTO> OrderBy( UserParams userParams, IEnumerable<ProductDTO> model)
        {
            Func<ProductDTO, Object> orderByFunc = null;

            switch (userParams.Orderby)
            {
                case "name":
                    orderByFunc = item => item.Name;
                    break;
                case "detail":
                    orderByFunc = item => item.Detail;
                    break;
                case "price":
                    orderByFunc = item => item.Price;
                    break;
                case "category":
                    orderByFunc = item => item.Category.Name;
                    break;
                default:
                    orderByFunc = item => item.Id;
                    break;
            }

            if (userParams.Order == "DESC")
                model = model.OrderByDescending(orderByFunc);
            else
                model = model.OrderBy(orderByFunc);

            return model;
        }

    }
}
