﻿namespace TTIStore.API.DTOs
{
    public class CategoryDTO
    {
        public long Id { get; set; }

        public string Name { get; set; }

    }
}
