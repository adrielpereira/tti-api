﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TTIStore.API.DTOs
{
    public class ProductDTO
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }

        public string Detail { get; set; }

        public long CategoryId { get; set; }

        public CategoryDTO Category { get; set; }

    }
}
