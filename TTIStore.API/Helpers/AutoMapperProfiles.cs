﻿using AutoMapper;
using TTIStore.API.DTOs;
using TTIStore.API.Models;
using TTIStore.Data.Models;

namespace TTIStore.API.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<Product, ProductDTO>();
            CreateMap<ProductDTO, Product>();
            CreateMap<Product, ProductViewModel>();
            CreateMap<ProductViewModel, Product>();

            CreateMap<Category, CategoryDTO>();
            CreateMap<CategoryDTO, Category>();

            CreateMap<SubcategoryDTO, Subcategory>();
            CreateMap<Subcategory, SubcategoryDTO>();

            CreateMap<SubcategoryViewModel, Subcategory>();
            CreateMap<Subcategory, SubcategoryViewModel>();


            CreateMap<ProductSubcategory, ProductSubcategoryViewModel>();
            CreateMap<ProductSubcategoryViewModel, ProductSubcategory>();
        }
    }
}
